<?php

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

require __DIR__ . '/vendor/autoload.php';

/**
 * Refresh Endpoint
 */
class Refresh implements MessageComponentInterface
{
    protected $clients;

    /**
     * Prepare clients object.
     */
    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    /**
     * When connections open add connection to clients
     *
     * @param ConnectionInterface $conn
     * @return void
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
    }

    /**
     * When message recived.
     *
     * @param ConnectionInterface $from
     * @param string $payload
     * @return void
     */
    public function onMessage(ConnectionInterface $from, $payload)
    {
        $payloadArray = json_decode($payload, true);

        $userId = $payloadArray['userId'];

        foreach ($this->clients as $client) {
            if ($from != $client) {
                $client->send($userId);
            }
        }
    }

    /**
     * When closing
     *
     * @param ConnectionInterface $conn
     * @return void
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
    }

    /**
     * When error, close connection.
     *
     * @param ConnectionInterface $conn
     * @param \Exception $e
     * @return void
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->close();
    }
}


// Run the server application through the WebSocket protocol on port 8009
$app = new Ratchet\App(getenv('SOCKET_SERVER_NAME'), '8080', '0.0.0.0');
$app->route('/', new Refresh, array('*'));
$app->run();
