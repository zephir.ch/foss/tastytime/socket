# Tasty Time Socket

The socket server for Tasty Time running on **port 8080**.

## ENV VARS

|var|required|description
|---|--------|-----------
|`SOCKET_SERVER_NAME`|yes|The server name the socket should listen on, example `socket.mytastytime.com`