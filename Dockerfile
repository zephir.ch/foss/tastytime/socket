FROM php:7.3-cli-alpine

COPY . /app

EXPOSE 8080

CMD ["php", "/app/index.php"]
